Instalación automatizada de GNU/Linux
=====================================
Un repositorio para archivos de automatización de instalación; tales como:

* kickstarts
* preseeds

Referencias
===========
* https://pykickstart.readthedocs.io/en/latest/kickstart-docs.html
* https://wiki.debian.org/DebianInstaller/Preseed
